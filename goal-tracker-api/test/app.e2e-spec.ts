import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { configApp } from './../src/main.config';

describe('Goals (e2e)', () => {
  const title = 'goal title';
  const description = 'goal description';
  const status = 'IN_PROGRESS';
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    configApp(app);

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('POST /goals | [201] Success', async () => {
    const res = await request(app.getHttpServer()).post('/goals').send({ title, description, status });

    expect(res.statusCode).toBe(201);
    expect(res.body.title).toBe(title);
    expect(res.body.id).toBeDefined();
    expect(res.body.description).toBe(description);
    expect(res.body.status).toBe(status);
    expect(res.body.createdAt).toBeDefined();
    expect(res.body.updatedAt).toBeDefined();
  });

  it('POST /goals | [201] Success (title only)', async () => {
    const res = await request(app.getHttpServer()).post('/goals').send({ title });

    expect(res.statusCode).toBe(201);
    expect(res.body.title).toBe(title);
  });

  it('POST /goals | [400] Validation Error(s)', async () => {
    const res = await request(app.getHttpServer()).post('/goals').send({});

    expect(res.statusCode).toBe(400);
    expect(res.body.statusCode).toBe(400);
    expect(res.body.error).toBe('Bad Request');
    expect(res.body.message).toBeInstanceOf(Array);
  });

  it('GET /goals | [200] Success', async () => {
    await request(app.getHttpServer()).post('/goals').send({ title });
    await request(app.getHttpServer()).post('/goals').send({ title });

    const res = await request(app.getHttpServer()).get('/goals');

    expect(res.statusCode).toBe(200);
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(2);
    expect(res.body[0].title).toBe(title);
    expect(res.body[1].title).toBe(title);
  });

  it('GET /goals | [200] Success (empty)', async () => {
    const res = await request(app.getHttpServer()).get('/goals');

    expect(res.statusCode).toBe(200);
    expect(res.body).toBeInstanceOf(Array);
    expect(res.body.length).toBe(0);
  });

  it('GET /goals/:id | [200] Success', async () => {
    const postRes = await request(app.getHttpServer()).post('/goals').send({ title });
    const id = postRes.body.id;

    const res = await request(app.getHttpServer()).get(`/goals/${id}`);

    expect(res.statusCode).toBe(200);
    expect(res.body.id).toBe(id);
    expect(res.body.title).toBe(title);
  });

  it('GET /goals/:id | [404] Not Found', async () => {
    const res = await request(app.getHttpServer()).get(`/goals/123`);

    expect(res.statusCode).toBe(404);
    expect(res.body.statusCode).toBe(404);
    expect(res.body.message).toBe('Not Found');
  });

  it('PATCH /goals/:id | [204] Success', async () => {
    const postRes = await request(app.getHttpServer()).post('/goals').send({ title });
    const id = postRes.body.id;

    const patchRes = await request(app.getHttpServer()).patch(`/goals/${id}`).send({ description });
    const getRes = await request(app.getHttpServer()).get(`/goals/${id}`);

    expect(patchRes.statusCode).toBe(204);
    expect(getRes.body.description).toBe(description);
  });

  it('PATCH /goals/:id | [404] Not Found', async () => {
    const res = await request(app.getHttpServer()).patch(`/goals/123`).send({ description });

    expect(res.statusCode).toBe(404);
    expect(res.body.statusCode).toBe(404);
    expect(res.body.message).toBe('Not Found');
  });

  it('PATCH /goals/:id | [400] Validation Error(s)', async () => {
    const postRes = await request(app.getHttpServer()).post('/goals').send({ title });
    const id = postRes.body.id;

    const res = await request(app.getHttpServer()).patch(`/goals/${id}`).send({ status: 'X' });

    expect(res.statusCode).toBe(400);
    expect(res.body.statusCode).toBe(400);
    expect(res.body.error).toBe('Bad Request');
    expect(res.body.message).toBeInstanceOf(Array);
  });

  it('DELETE /goals/:id | [204] Success', async () => {
    const postRes = await request(app.getHttpServer()).post('/goals').send({ title });
    const id = postRes.body.id;

    const patchRes = await request(app.getHttpServer()).delete(`/goals/${id}`);
    const getRes = await request(app.getHttpServer()).get(`/goals/${id}`);

    expect(patchRes.statusCode).toBe(204);
    expect(getRes.statusCode).toBe(404);
  });

  it('DELETE /goals/:id | [404] Not Found', async () => {
    const res = await request(app.getHttpServer()).delete(`/goals/123`);

    expect(res.statusCode).toBe(404);
    expect(res.body.statusCode).toBe(404);
    expect(res.body.message).toBe('Not Found');
  });
});
