import { NodeEnvironment } from './env';
import { validate } from './env.validation';

describe('EnvVaildation', () => {
  const requiredEnvVars = {
    NODE_ENV: NodeEnvironment.Development,
    DB_HOST: 'host',
    DB_PORT: '3456',
    DB_NAME: 'database',
    DB_USERNAME: 'username',
    DB_PASSWORD: 'pasword',
  };

  it(`Should not throw any error if all required env variables are present and valid`, () => {
    const act = () => validate(requiredEnvVars);

    expect(act).not.toThrow();
  });

  it(`Should have a default 'PORT' env variable`, () => {
    const validatedEnvVars = validate(requiredEnvVars);

    expect(validatedEnvVars.PORT).toBe('3000');
  });

  it.each(Object.keys(requiredEnvVars))(`Should throw an error if %p env variable is missing`, (envVar) => {
    const act = () => validate({ ...requiredEnvVars, [envVar]: undefined });

    expect(act).toThrow(`property ${envVar} has failed`);
  });
});
