import { AutoMap } from '@automapper/classes';
import { GoalStatus } from '../entities/goal.entity';

export class GetGoalDto {
  @AutoMap() id: string;
  @AutoMap() title: string;
  @AutoMap() description: string;
  @AutoMap() status: GoalStatus;
  @AutoMap() createdAt: Date;
  @AutoMap() updatedAt: Date;
}
