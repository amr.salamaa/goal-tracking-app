import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnvironmentVariables, NodeEnvironment } from '../config/env';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService<EnvironmentVariables>) => {
        const env = configService.get<NodeEnvironment>('NODE_ENV');
        const isDevEnv = env === NodeEnvironment.Development;
        const isTestEnv = env === NodeEnvironment.Test;

        if (isTestEnv) {
          return {
            type: 'sqlite',
            database: ':memory:',
            autoLoadEntities: true,
            synchronize: true,
          };
        }

        return {
          type: 'mysql',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          database: configService.get('DB_NAME'),
          username: configService.get('DB_USERNAME'),
          password: configService.get('DB_PASSWORD'),
          autoLoadEntities: true,
          logging: isDevEnv,
          synchronize: true, // TODO: use migrations in prod instead, ignored for the simplicty of the task
        };
      },
    }),
  ],
  exports: [TypeOrmModule],
})
export class DatabaseModule {}
