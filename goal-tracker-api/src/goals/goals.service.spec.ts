import { Test, TestingModule } from '@nestjs/testing';
import { getMapperToken } from '@automapper/nestjs';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GoalsService } from './goals.service';
import { Goal } from './entities/goal.entity';
import { CreateGoalDto } from './dto/create-goal.dto';
import { GetGoalDto } from './dto/get-goal.dto';
import { NotFoundException } from '@nestjs/common';
import { UpdateGoalDto } from './dto/update-goal.dto';

describe('GoalsService', () => {
  const mockId = '123';
  const mockCreateGoalDto = { title: 'new goal' };
  const mockUpdateGoalDto = { description: 'xyz description 123' };
  const mockGetGoalDto = { id: mockId, ...mockCreateGoalDto };
  const mockGoal = { ...mockGetGoalDto };

  const mockGoalsRepository = {
    save: jest.fn(),
    find: jest.fn(),
    findOneBy: jest.fn(),
    update: jest.fn(),
    delete: jest.fn(),
  };
  const mockMapper = {
    map: jest.fn(),
    mapArray: jest.fn(),
  };
  let goalsService: GoalsService;

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GoalsService,
        {
          provide: getRepositoryToken(Goal),
          useValue: mockGoalsRepository,
        },
        {
          provide: getMapperToken(),
          useValue: mockMapper,
        },
      ],
    }).compile();

    goalsService = module.get<GoalsService>(GoalsService);
  });

  it(`should be defined`, () => {
    expect(goalsService).toBeDefined();
  });

  it(`should create and return the newly created goal dto when "create" is called`, async () => {
    mockMapper.map.mockReturnValue(mockGoal);
    mockMapper.map.mockReturnValue(mockGetGoalDto);
    mockGoalsRepository.save.mockResolvedValueOnce(mockGoal);

    const createdGoal = await goalsService.create(mockCreateGoalDto);

    expect(createdGoal).toBe(mockGetGoalDto);
    expect(mockGoalsRepository.save).toHaveBeenCalledTimes(1);
    expect(mockGoalsRepository.save).toHaveBeenNthCalledWith(1, mockGoal);
  });

  it(`should map from dto to entity and vice versa when "create" is called`, async () => {
    mockGoalsRepository.save.mockResolvedValueOnce(mockGoal);

    await goalsService.create(mockCreateGoalDto);

    expect(mockMapper.map).toHaveBeenCalledTimes(2);
    expect(mockMapper.map).toHaveBeenNthCalledWith(1, mockCreateGoalDto, CreateGoalDto, Goal);
    expect(mockMapper.map).toHaveBeenNthCalledWith(2, mockGoal, Goal, GetGoalDto);
  });

  it(`should return goal dtos ordered by asc "createdAt" field when "findAll" is called`, async () => {
    const goals = [mockGoal];
    const mockGetGoalDtos = [mockGetGoalDto];
    mockMapper.mapArray.mockReturnValue(mockGetGoalDtos);
    mockGoalsRepository.find.mockResolvedValueOnce(goals);

    const goalsDtos = await goalsService.findAll();

    expect(goalsDtos).toBe(mockGetGoalDtos);
    expect(mockGoalsRepository.find).toHaveBeenCalledTimes(1);
    expect(mockGoalsRepository.find).toHaveBeenNthCalledWith(1, { order: { createdAt: `ASC` } });
  });

  it(`should map from entity to dto when "findAll" is called`, async () => {
    const goals = [mockGoal];
    mockGoalsRepository.find.mockResolvedValueOnce(goals);

    await goalsService.findAll();

    expect(mockMapper.mapArray).toHaveBeenCalledTimes(1);
    expect(mockMapper.mapArray).toHaveBeenNthCalledWith(1, goals, Goal, GetGoalDto);
  });

  it(`should return the goal dto when "findOne" is called and the goal exists`, async () => {
    mockMapper.map.mockReturnValue(mockGetGoalDto);
    mockGoalsRepository.findOneBy.mockResolvedValueOnce(mockGoal);

    const goalDto = await goalsService.findOne(mockId);

    expect(goalDto).toBe(mockGetGoalDto);
    expect(mockGoalsRepository.findOneBy).toHaveBeenCalledTimes(1);
    expect(mockGoalsRepository.findOneBy).toHaveBeenNthCalledWith(1, { id: mockId });
  });

  it(`should map from entity to dto when "findOne" is called and the goal exists`, async () => {
    mockGoalsRepository.findOneBy.mockResolvedValueOnce(mockGoal);

    await goalsService.findOne(mockId);

    expect(mockMapper.map).toHaveBeenCalledTimes(1);
    expect(mockMapper.map).toHaveBeenNthCalledWith(1, mockGoal, Goal, GetGoalDto);
  });

  it(`should throw NotFoundException when "findOne" is called and the goal doesn't exist`, async () => {
    mockGoalsRepository.findOneBy.mockResolvedValueOnce(null);

    const act = async () => goalsService.findOne(mockId);

    await expect(act).rejects.toThrow(NotFoundException);
  });

  it(`should update the goal when "update" is called and the goal exists`, async () => {
    mockMapper.map.mockReturnValue(mockGoal);
    mockGoalsRepository.update.mockResolvedValueOnce({ affected: 1 });

    await goalsService.update(mockId, mockUpdateGoalDto);

    expect(mockGoalsRepository.update).toHaveBeenCalledTimes(1);
    expect(mockGoalsRepository.update).toHaveBeenNthCalledWith(1, { id: mockId }, mockGoal);
  });

  it(`should map from dto to entity when "update" is called`, async () => {
    mockGoalsRepository.update.mockResolvedValueOnce({ affected: 1 });

    await goalsService.update(mockId, mockUpdateGoalDto);

    expect(mockMapper.map).toHaveBeenCalledTimes(1);
    expect(mockMapper.map).toHaveBeenNthCalledWith(1, mockUpdateGoalDto, UpdateGoalDto, Goal);
  });

  it(`should throw NotFoundException when "update" is called and the goal doesn't exist`, async () => {
    mockGoalsRepository.update.mockResolvedValueOnce({ affected: 0 });

    const act = async () => goalsService.update(mockId, mockUpdateGoalDto);

    await expect(act).rejects.toThrow(NotFoundException);
  });

  it(`should delete the goal when "remove" is called and the goal exists`, async () => {
    mockGoalsRepository.delete.mockResolvedValueOnce({ affected: 1 });

    await goalsService.remove(mockId);

    expect(mockGoalsRepository.delete).toHaveBeenCalledTimes(1);
    expect(mockGoalsRepository.delete).toHaveBeenNthCalledWith(1, mockId);
  });

  it(`should throw NotFoundException when "remove" is called and the goal doesn't exist`, async () => {
    mockGoalsRepository.delete.mockResolvedValueOnce({ affected: 0 });

    const act = async () => goalsService.remove(mockId);

    await expect(act).rejects.toThrow(NotFoundException);
  });
});
