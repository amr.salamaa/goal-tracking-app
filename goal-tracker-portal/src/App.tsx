import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { Icon } from "semantic-ui-react";
import "./App.css";
import GoalForm from "./pages/GoalForm/GoalForm";
import Home from "./pages/Home/Home";

function App() {
  return (
    <Router>
      <div className="app">
        <h1 className="header">
          <Icon name="target" size="large" />
          Goal Tracking App
        </h1>
        <div>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/new" element={<GoalForm />} />
            <Route path="/goals/:goalId" element={<GoalForm />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
