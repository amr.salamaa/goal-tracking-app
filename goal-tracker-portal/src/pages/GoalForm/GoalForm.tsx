import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Form, Icon, Message } from "semantic-ui-react";
import { API_BASE_URL } from "../../config";
import { GoalStatus, IGoal } from "../../models/goal";
import "./GoalForm.css";

export default function GoalForm() {
  const navigate = useNavigate();
  const { goalId } = useParams();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(goalId !== undefined);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState<GoalStatus>(GoalStatus.PENDING);

  useEffect(() => {
    if (!goalId) return;
    axios
      .get<IGoal>(`${API_BASE_URL}/goals/${goalId}`)
      .then(({ data }) => {
        setTitle(data.title);
        setDescription(data.description);
        setStatus(data.status);
      })
      .catch(() => navigate("/"))
      .finally(() => setLoading(false));
  }, [goalId, navigate]);

  const save = async () => {
    setLoading(true);
    const goal = { title, description, status };
    const req = goalId
      ? axios.patch(`${API_BASE_URL}/goals/${goalId}`, goal)
      : axios.post(`${API_BASE_URL}/goals`, goal);
    req
      .then(() => navigate("/"))
      .catch((err) => {
        setError(err?.response?.data?.message || err.message || "Something Went Wrong!");
        console.log();
      })
      .finally(() => setLoading(false));
  };

  const remove = async () => {
    if (!goalId) return;
    setLoading(true);
    axios
      .delete(`${API_BASE_URL}/goals/${goalId}`)
      .then(() => navigate("/"))
      .finally(() => setLoading(false));
  };

  return (
    <div className="goal-form">
      <div className="header">
        <Button icon basic labelPosition="left" size="tiny" onClick={() => navigate("/")}>
          <Icon name="cancel" /> Cancel
        </Button>
        <Button.Group size="tiny">
          {goalId && (
            <Button icon basic negative labelPosition="left" disabled={loading} onClick={remove}>
              <Icon name="trash" /> Delete
            </Button>
          )}
          <Button icon labelPosition="left" disabled={title.length < 3} onClick={save}>
            <Icon name="save" /> Save
          </Button>
        </Button.Group>
      </div>

      <Form loading={loading}>
        <Form.Input label="Title" placeholder="Title" value={title} onChange={(e) => setTitle(e.target.value)} />
        <Form.TextArea
          value={description}
          label="Description"
          placeholder="Keep it concise ..."
          onChange={(e) => setDescription(e.target.value)}
        />

        <Button.Group fluid>
          <Button active={status === GoalStatus.PENDING} onClick={(_) => setStatus(GoalStatus.PENDING)}>
            <Icon name="bullseye" /> Pending
          </Button>
          <Button active={status === GoalStatus.IN_PROGRESS} onClick={(_) => setStatus(GoalStatus.IN_PROGRESS)}>
            <Icon name="clock outline" /> In Progress
          </Button>
          <Button active={status === GoalStatus.DONE} onClick={(_) => setStatus(GoalStatus.DONE)}>
            <Icon name="check" /> Done
          </Button>
        </Button.Group>
      </Form>

      {!loading && error && <Message error>{error}</Message>}
    </div>
  );
}
