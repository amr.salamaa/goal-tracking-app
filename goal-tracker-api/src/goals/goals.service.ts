import { Mapper } from '@automapper/core';
import { InjectMapper } from '@automapper/nestjs';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateGoalDto } from './dto/create-goal.dto';
import { GetGoalDto } from './dto/get-goal.dto';
import { UpdateGoalDto } from './dto/update-goal.dto';
import { Goal } from './entities/goal.entity';

@Injectable()
export class GoalsService {
  constructor(
    @InjectRepository(Goal) private readonly goalsRepository: Repository<Goal>,
    @InjectMapper() private readonly mapper: Mapper,
  ) {}

  async create(createGoalDto: CreateGoalDto) {
    const goal = this.mapper.map(createGoalDto, CreateGoalDto, Goal);
    const createdGoal = await this.goalsRepository.save(goal);
    return this.mapper.map(createdGoal, Goal, GetGoalDto);
  }

  async findAll() {
    const goals = await this.goalsRepository.find({ order: { createdAt: 'ASC' } });
    return this.mapper.mapArray(goals, Goal, GetGoalDto);
  }

  async findOne(id: string) {
    const goal = await this.goalsRepository.findOneBy({ id });
    if (!goal) throw new NotFoundException();
    return this.mapper.map(goal, Goal, GetGoalDto);
  }

  async update(id: string, updateGoalDto: UpdateGoalDto) {
    const goal = this.mapper.map(updateGoalDto, UpdateGoalDto, Goal);
    const result = await this.goalsRepository.update({ id }, goal);
    if (!result.affected) throw new NotFoundException();
  }

  async remove(id: string) {
    const result = await this.goalsRepository.delete(id);
    if (!result.affected) throw new NotFoundException();
  }
}
