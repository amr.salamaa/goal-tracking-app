import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Icon, Message, Segment, Step } from "semantic-ui-react";
import { API_BASE_URL } from "../../config";
import { GoalStatus, IGoal } from "../../models/goal";
import "./Home.css";

export default function Home() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [goals, setGoals] = useState<IGoal[]>([]);

  useEffect(() => {
    axios
      .get(`${API_BASE_URL}/goals`)
      .then((response) => {
        setGoals(response.data);
      })
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));
  }, []);

  return (
    <div className="home-page">
      <div className="header">
        <div>Goals: {goals.length}</div>
        <Button icon labelPosition="right" size="tiny" basic onClick={() => navigate("/new")}>
          <Icon name="plus" /> New
        </Button>
      </div>

      {loading && <Segment loading basic></Segment>}

      {!loading && error && <Message error>{error}</Message>}

      {!loading && !error && goals.length === 0 && <div>No goals yet!</div>}

      {!loading && !error && goals.length > 0 && (
        <Step.Group vertical fluid>
          {goals.map((goal) => (
            <Step
              className="goal-item"
              key={goal.id}
              completed={goal.status === GoalStatus.DONE}
              onClick={() => navigate(`/goals/${goal.id}`)}
            >
              <Icon name={goal.status === GoalStatus.IN_PROGRESS ? "clock outline" : "bullseye"} />
              <Step.Content>
                <Step.Title>{goal.title}</Step.Title>
                {goal.description && <Step.Description>{goal.description}</Step.Description>}
              </Step.Content>
            </Step>
          ))}
        </Step.Group>
      )}
    </div>
  );
}
