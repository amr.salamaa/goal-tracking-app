import { Test, TestingModule } from '@nestjs/testing';
import { GoalsController } from './goals.controller';
import { GoalsService } from './goals.service';

describe('GoalsController', () => {
  const mockId = '123';
  const mockCreateGoalDto = { title: 'new goal' };
  const mockGetGoalDto = { id: mockId, ...mockCreateGoalDto };

  const mockGoalsService = {
    create: jest.fn(),
    findAll: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
    remove: jest.fn(),
  };
  let goalsController: GoalsController;

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [GoalsController],
      providers: [
        {
          provide: GoalsService,
          useValue: mockGoalsService,
        },
      ],
    }).compile();

    goalsController = module.get<GoalsController>(GoalsController);
  });

  it('should be defined', () => {
    expect(goalsController).toBeDefined();
  });

  it('should return the newly created goal when "create" is called', async () => {
    mockGoalsService.create.mockResolvedValueOnce(mockGetGoalDto);

    const createdGoal = await goalsController.create(mockCreateGoalDto);

    expect(createdGoal).toBe(mockGetGoalDto);
    expect(mockGoalsService.create).toHaveBeenCalledTimes(1);
    expect(mockGoalsService.create).toHaveBeenCalledWith(mockCreateGoalDto);
  });

  it('should return the goals when "findAll" is called', async () => {
    const mockGoals = [mockGetGoalDto];
    mockGoalsService.findAll.mockResolvedValueOnce(mockGoals);

    const goals = await goalsController.findAll();

    expect(goals).toBe(mockGoals);
    expect(mockGoalsService.findAll).toHaveBeenCalledTimes(1);
    expect(mockGoalsService.findAll).toHaveBeenCalledWith();
  });

  it('should return the goal when "findOne" is called', async () => {
    mockGoalsService.findOne.mockResolvedValueOnce(mockGetGoalDto);

    const goal = await goalsController.findOne(mockId);

    expect(goal).toBe(mockGetGoalDto);
    expect(mockGoalsService.findOne).toHaveBeenCalledTimes(1);
    expect(mockGoalsService.findOne).toHaveBeenCalledWith(mockId);
  });

  it('should update the goal when "update" is called', async () => {
    const mcokUpdateGoalDto = { title: 'update' };

    await goalsController.update(mockId, mcokUpdateGoalDto);

    expect(mockGoalsService.update).toHaveBeenCalledTimes(1);
    expect(mockGoalsService.update).toHaveBeenCalledWith(mockId, mcokUpdateGoalDto);
  });

  it('should remove the goal when "remove" is called', async () => {
    await goalsController.remove(mockId);

    expect(mockGoalsService.remove).toHaveBeenCalledTimes(1);
    expect(mockGoalsService.remove).toHaveBeenCalledWith(mockId);
  });
});
