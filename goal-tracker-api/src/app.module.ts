import { classes as classesAutomapperStrategy } from '@automapper/classes';
import { AutomapperModule } from '@automapper/nestjs';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { validate as envValidate } from './config/env.validation';
import { DatabaseModule } from './database/database.module';
import { GoalsModule } from './goals/goals.module';

@Module({
  imports: [
    ConfigModule.forRoot({ validate: envValidate }),
    AutomapperModule.forRoot({ strategyInitializer: classesAutomapperStrategy() }),
    DatabaseModule,
    GoalsModule,
  ],
})
export class AppModule {}
