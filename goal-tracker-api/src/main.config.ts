import { INestApplication, ValidationPipe } from '@nestjs/common';

export function configApp(app: INestApplication) {
  app.enableCors();
  app.getHttpAdapter().getInstance().disable('x-powered-by');
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );
}
