# Goal Tracking App

A simple CRUD application containing a web portal consuming a service layer using a database to store the data.


## How To

- Make sure to create a `.env` file with the same variables defined in the example file `.env.example`

```sh
$ docker-compose up
```


## Screenshots

<img src="docs/Screenshot-1.png" width="480" title="Design">
<img src="docs/Screenshot-2.png" width="480" title="Design">
<img src="docs/Screenshot-3.png" width="480" title="Design">
