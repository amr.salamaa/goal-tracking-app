export enum GoalStatus {
  PENDING = "PENDING",
  IN_PROGRESS = "IN_PROGRESS",
  DONE = "DONE",
}

export interface IGoal {
  id: string;
  title: string;
  description: string;
  status: GoalStatus;
  createdAt: Date;
  updatedAt: Date;
}
