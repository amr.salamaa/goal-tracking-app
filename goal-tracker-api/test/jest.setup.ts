import 'reflect-metadata';
import { NodeEnvironment } from '../src/config/env';

process.env.NODE_ENV = NodeEnvironment.Test;
