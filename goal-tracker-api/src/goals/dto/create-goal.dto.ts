import { AutoMap } from '@automapper/classes';
import { IsEnum, IsOptional, IsString, Length, MaxLength } from 'class-validator';
import { GoalStatus } from '../entities/goal.entity';

export class CreateGoalDto {
  @AutoMap()
  @IsString()
  @Length(3, 32)
  title: string;

  @AutoMap()
  @IsString()
  @MaxLength(255)
  @IsOptional()
  description?: string;

  @AutoMap()
  @IsEnum(GoalStatus)
  @IsOptional()
  status?: GoalStatus;
}
