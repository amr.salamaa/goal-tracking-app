import { AutoMap } from '@automapper/classes';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

export enum GoalStatus {
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE',
}

@Entity({
  name: 'goals',
})
export class Goal {
  @AutoMap()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @AutoMap()
  @Column({ length: 32 })
  title: string;

  @AutoMap()
  @Column({ length: 255, default: '' })
  description: string;

  @AutoMap()
  @Column({
    type: 'simple-enum',
    enum: GoalStatus,
    default: GoalStatus.PENDING,
  })
  status: GoalStatus;

  @AutoMap()
  @CreateDateColumn()
  createdAt: Date;

  @AutoMap()
  @UpdateDateColumn()
  updatedAt: Date;
}
